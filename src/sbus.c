/*
2019 12 21 Sulincix (Ali Rıza KESKİN)

create_node : creating new fifo file
delete_node : remove target fifo file
sbus_puts   : send string to fifi
sbus_gets   : read string from fifo

set_node    : set target fifo file

*/

#include <sys/stat.h>
#include <stdio.h>
#include <stdarg.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

const char* rname;
void set_node(const char *name){
	rname=name;
}
void delete_node(const char* name){
	unlink(name);
}

int create_node(const char *name){
	return mkfifo(name, 0644);
}
const char *sbus_gets(int max){
	char* str=malloc(max);
	int rnode=open(rname,O_RDONLY);
    	read(rnode,str,max);    
	close(rnode);
	return str;
}

void sbus_puts(char *str){
	int rnode=open(rname,O_WRONLY);
    	write(rnode,str,strlen(str)+1);
	close(rnode);
}
