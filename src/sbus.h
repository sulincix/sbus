/*
2019 12 21 Sulincix (Ali Rıza KESKİN)

create_node : creating new fifo file
delete_node : remove target fifo file
sbus_puts   : send string to fifi
sbus_gets   : read string from fifo

set_node    : set target fifo file

*/

int create_node(const char *name);
char* sbus_gets(int max);
void set_node(const char *name);
void sbus_puts(char *str);
void delete_node(const char* name);
