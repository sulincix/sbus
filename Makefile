all: clean build

clean:
	rm -rf build || true
build:
	mkdir build || true
	$(CC) -fPIC -c src/sbus.c -o build/sbus.o
	$(CC) -shared -o build/libsbus.so build/sbus.o
install:
	mkdir -p $(DESTDIR)/lib || true
	mkdir -p $(DESTDIR)/include || true
	install build/libsbus.so $(DESTDIR)/lib/libsbus.so
	install src/sbus.h $(DESTDIR)/include/sbus.h

