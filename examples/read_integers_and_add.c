#include <sbus.h>
#include <stdio.h>
int main(){
    create_node("test_node");
    set_node("test_node");
    while(1){
        char *value=sbus_gets(1024);
        int i,j;
        char c;
        sscanf(value,"%d %c %d",&i,&c,&j);
        if(c=='+'){
            printf("%d\n",(i+j));
        }else if(c=='-'){
            printf("%d\n",(i-j));
        }else if(c=='*'){
            printf("%d\n",(i*j));
        }else if(c=='/'){
            printf("%d\n",(i/j));
        }
    }
}
